// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  vscode.languages.registerInlineCompletionItemProvider(
    { scheme: 'file', language: 'typescript' },
    {
      async provideInlineCompletionItems(
        document: vscode.TextDocument,
        position: vscode.Position,
        context: vscode.InlineCompletionContext,
        token: vscode.CancellationToken
      ): Promise<vscode.InlineCompletionItem[]> {
        const items: vscode.InlineCompletionItem[] = [
          { insertText: 'Hello World' },
          { insertText: 'Hello World 1' },
          { insertText: 'Hello World 2' },
        ];

        // we only care about the case when IntelliSense is open
        if (!context.selectedCompletionInfo) {
          return [];
        }

        // the items have to start with the IntelliSense text and they replace the original intellisense range
        const result = items.map((i) => ({
          insertText: `${context.selectedCompletionInfo!.text} ${i.insertText}`,
          range: context.selectedCompletionInfo!.range,
        }));

        // only return one item if the completion is automatically invoked
        if (
          context.triggerKind === vscode.InlineCompletionTriggerKind.Automatic
        ) {
          return [result[0]];
        }
        return result;
      },
    }
  );
}
